﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FloatVal", menuName = "Scriptable Objects/Primitives/Float Value", order = 1)]
public class Float : ScriptableObject {
    public float value;
}
