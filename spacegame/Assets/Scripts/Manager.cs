﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager : MonoBehaviour {
    private bool paused;

    private void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    private void Update() {
        if (SceneManager.GetActiveScene().name == "Game") {
            if (Input.GetKeyDown(KeyCode.Escape))
                SceneManager.LoadScene("Menu");
            if (Input.GetKeyDown(KeyCode.R)) {
                SceneManager.LoadScene("Game");
                Time.timeScale = 1f;
            }
        }
    }

    public void Play() {
        SceneManager.LoadScene("Game");
    }

    public void Quit() {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
