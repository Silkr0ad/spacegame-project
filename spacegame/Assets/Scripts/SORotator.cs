﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SORotator : MonoBehaviour {
    public Float angle;

    private void Update() {
        transform.Rotate(0f, 0f, angle.value * Time.deltaTime);
    }
}
