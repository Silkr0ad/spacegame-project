﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "IntVal", menuName = "Scriptable Objects/Primitives/Integer Value", order = 0)]
public class Int : ScriptableObject {
    public int value;
}
