﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController2D : MonoBehaviour {
    public float stayAheadDistance;
    public Transform playerTransform;
    public Vector3 offset;
    public float smoothTime = 0.5f;

    private Vector3 oldUp;
    private Vector3 oldDistance;

    private void Start() {
        oldUp = Vector3.up;
    }

    private void Update() {
        if (Input.GetButton("Fire1"))
            StayAhead();
        else if (Mathf.Abs(Input.GetAxis("Horizontal")) >= 0.01)
            KeepCameraPosition();

        // reset camera position; for debugging only
        if (Input.GetKeyDown(KeyCode.F))
            transform.localPosition = Vector3.zero + offset;
    }

    private void KeepCameraPosition() {
        float angle = Vector3.Angle(oldUp, playerTransform.up);
        float upAngle = Vector3.Angle(Vector3.up, oldUp);

        if (oldUp.x < 0)
            upAngle = -upAngle;

        Vector3 adjustedPTUp = Quaternion.Euler(0f, 0f, upAngle) * playerTransform.up;

        if (adjustedPTUp.x < 0)
            angle = -angle;

        Vector3 stayAheadDirection = Quaternion.Euler(0f, 0f, angle) * Vector3.up;
        transform.localPosition = stayAheadDirection * oldDistance.magnitude + offset;
        Debug.Log(stayAheadDirection + " | " + oldDistance.magnitude + " | " + ((Vector2)transform.localPosition).magnitude);
    }

    private void StayAhead() {
        Vector3 targetPos = Vector3.up * stayAheadDistance + offset;
        transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, smoothTime * Time.deltaTime);
        oldUp = playerTransform.up;
        oldDistance = transform.localPosition - offset;
    }
}
