﻿Shader "Sprites/Heat Distortion Mk. III"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_Filter("Filter Texture", 2D) = "white" {}
		_Noise("Noise Texture", 2D) = "white" {}
		_Strength("Strength", float) = 1.0
		_Speed("Distort Speed (lower = faster)", float) = 1.0
		_Color("Tint", Color) = (1, 1, 1, 0.0)
	}

		SubShader
	{
		Cull Off

		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
			"DisableBatching" = "True"
		}

		GrabPass
		{
		  "_BackgroundTexture"
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			//sampler2D _MainTex;
			sampler2D _Filter;
			sampler2D _Noise;
			sampler2D _BackgroundTexture;
			float _Strength;
			float _Speed;
			fixed4 _Color;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 grabPos : TEXCOORD1;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.grabPos = ComputeGrabScreenPos(o.vertex);
				return o;
			}

			
			fixed4 frag(v2f i) : COLOR
			{
				float filter = tex2D(_Filter, i.uv).rgb;
				float2 disp = tex2D(_Noise, i.grabPos + _Time.a / _Speed).xy;
				disp = ((disp * 2) - 1) * _Strength * filter;
				half4 tint = _Color * _Color.a;
				return tex2Dproj(_BackgroundTexture, i.grabPos + float4(disp.x, disp.y, 0, 0)) + tint * filter;
			}

			ENDCG
		}
	}
}