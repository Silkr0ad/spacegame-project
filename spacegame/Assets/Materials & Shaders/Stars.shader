﻿Shader "Unlit/Stars"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Shade of Black", Color) = (0, 0, 0, 1.0)
		_Threshold("Threshold", float) = 0.5
		_SecondThreshold("Threshold", float) = 0.002
		_SecondBrightness("Secondary Star Brightness", float) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Color;
			float _Threshold;
			float _SecondThreshold;
			float _SecondBrightness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 col = _Color;
				float noise = (frac(sin(dot(i.uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
				if (noise > _Threshold)
					col.rgb += noise;
				else if (noise > _Threshold - _SecondThreshold)
					col.rgb += noise * _SecondBrightness;
				return col;
            }
            ENDCG
        }
    }
}
