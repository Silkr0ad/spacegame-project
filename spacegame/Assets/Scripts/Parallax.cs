﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {
    public float lag;
    private Transform cam;
    private Vector3 lastPos;

    private void Awake() {
        cam = Camera.main.transform;
    }

    private void Start() {
        lastPos = cam.position;
        StartCoroutine(ParallaxFollow());
    }

    private IEnumerator ParallaxFollow() {
        while (true) {
            yield return new WaitForEndOfFrame();
            Vector2 distance = cam.position - lastPos;
            transform.position -= (Vector3)distance * lag;
            lastPos = cam.position;
        }
    }
}
