﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController2D))]
public class PlayerMovement : MonoBehaviour {
    // references
    private CharacterController2D cc;
    public AudioSource engineAudio;
    public ParticleSystem particles;
    public GameObject tutPanel;

    //private fields
    private float axis;
    private bool fired1;
    private bool won;

    private void Awake() {
        cc = GetComponent<CharacterController2D>();
    }
    private void Start() {
        particles.Stop();
        tutPanel.SetActive(true);
    }

    private void Update() {
        if (!won) {
            // updating fields
            axis = Input.GetAxis("Horizontal");
            fired1 = Input.GetButton("Fire1");

            // invoking one-off functions
            if (Input.GetButtonDown("Fire1")) {
                tutPanel.SetActive(false);
                particles.Play();
                engineAudio.Play();
            } else if (Input.GetButtonUp("Fire1")) {
                particles.Stop();
                engineAudio.Pause();
            }
        }
    }

    private void FixedUpdate() {
        if (fired1 && Mathf.Abs(axis) > 0) {
            cc.SetMolassesActive(true);
            cc.Rotate(axis, true);
        } else if (Mathf.Abs(axis) > 0) {
            cc.Rotate(axis, false);
        }

        if (fired1) {
            cc.Accelerate();
        }

        cc.SetMolassesActive(false);

        cc.LimitSpeed();
    }

    public void Won() {
        won = true;
    }
}
