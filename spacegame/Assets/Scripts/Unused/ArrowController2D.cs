﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController2D : MonoBehaviour {
    public Transform target;
    public Transform targeter;
    [Range(0f, 1f)] public float radius = 0.7f;
    public float hidePadding = 3f;

    private Camera cam;
    private SpriteRenderer rend;

    private Vector2 difference;

    private void Awake() {
        cam = Camera.main;
        rend = GetComponent<SpriteRenderer>();
    }

    private void Start() {
        transform.position = PositionArrow();
        transform.rotation = RotateArrow();
        StartCoroutine(AdjustArrow());
    }

    // The arrow kept stuttering and LateUpdate() wasn't cutting it, so I used WaitForEndOfFrame() on a whim.
    // It worked. Huh.
    private IEnumerator AdjustArrow() {
        while (true) {
            yield return new WaitForEndOfFrame();
            transform.position = PositionArrow();
            transform.rotation = RotateArrow();
        }
    }

    // TODO fix; not working
    //private void ClampArrow() {
    //    float x = transform.position.x;
    //    float y = transform.position.y;

    //    Mathf.Clamp(x, -(cam.orthographicSize * cam.aspect - 2f), cam.orthographicSize * cam.aspect - 2f);
    //    Mathf.Clamp(y, -(cam.orthographicSize - 2f), cam.orthographicSize - 2f);

    //    transform.position = new Vector2(x, y);
    //}

    private void ShowArrow() {
        rend.enabled = true;
    }

    private void HideArrow() {
        rend.enabled = false;
    }

    private Vector3 PositionArrow() {
        difference = target.position - targeter.position;
        return (Vector2)targeter.position + difference.normalized * cam.orthographicSize * radius;
    }

    private Quaternion RotateArrow() {
        float angle = Vector2.Angle(Vector2.right, difference);
        if (difference.y < 0)
            angle = -angle;
        return Quaternion.Euler(0f, 0f, angle);
    }
}
