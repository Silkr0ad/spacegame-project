﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeKeeper : MonoBehaviour {
    public Text timer;
    [HideInInspector] public string time;

    private bool keepCounting = true;

    private void Update() {
        if (keepCounting) {
            int minutes = 0;
            int seconds = (int)Time.timeSinceLevelLoad;
            float milliseconds = (Time.timeSinceLevelLoad - seconds) * 1000;
            milliseconds = (int)(Mathf.Round(milliseconds * 100f) / 100f);

            if (seconds >= 60) {
                minutes++;
                seconds -= 60;
            }

            time = minutes.ToString() + ":" + seconds.ToString() + ":" + milliseconds.ToString();
            timer.text = time;
        }
    }

    public void HideTimer() {
        timer.text = "";
        keepCounting = false;
    }
}
