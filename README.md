# Terminal Velocity

Inspired by Luftrausers and Power of Ten. Worked out like neither.

Picked "a general spacegame" as the topic for the project, which backfired horribly when I realized fast movement and 2D do not mesh together. Most of the development time was spent looking for ideas that would make this work without changing the topic (which was forbidden). No such ideas were found, so I finished the game as it was in a the last 1-2 days. I'm not proud of it, but it is what it is.

Credit for external assets is given below. What isn't credited has been made by myself.

*Note: Trying to upload a build of the game ended up in 'fatal: the remote end hung up unexpectedly'. As such, I opted not to.* 

![Screenshot](screenshot.png)

## Credits

### Textures

Displacement noise texture taken from [Dan John Moran's site.](http://danjohnmoran.com/Shaders/102/Displacements.png) (Thanks, Makin' Stuff Look Good!)

Indicator arrow from Nucleo.

Finish line from http://www.pngmart.com/image/31919

Walls/platforms from the "industrial tileset" retrieved from https://0x72.itch.io/16x16-industrial-tileset by 0x72 (otherwise known as Robert?) (2017).

### Code

Smoothing formula used as the ratio for Lerp() in TargetFollow.cs from https://forum.unity.com/threads/how-to-smooth-damp-towards-a-moving-target-without-causing-jitter-in-the-movement.130920/#post-883852 by George Foot (2012).

Pixelation shader from the tutorial https://www.youtube.com/watch?v=9bTFVaKGIIQ by World of Zero (2017).

### Audio

In-game music (Railjet Long Seamless) from https://opengameart.org/content/railjet-long-seamless-loop by qubodup (2013).

Menu music (Dark Intro) from https://opengameart.org/content/dark-intro by Nikke (2012).

Friction/scraping sound from http://soundbible.com/2198-Ice-Skating.html by Daniel Simion (2017).

Explosion sound from https://opengameart.org/content/chunky-explosion by Joth (2016).

Engine sound from https://opengameart.org/content/engine-sound by Kurt (2011).

### Other

Menu font from https://www.dafont.com/c-c-red-alert-inet.font