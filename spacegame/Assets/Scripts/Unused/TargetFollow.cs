﻿using UnityEngine;

public class TargetFollow : MonoBehaviour {
    public Transform target;
    public Vector3 offset;
    [Tooltip("No 'catch-up'. Have the game object follow the target exactly. Overrides 'linear'.")]
    public bool exact = true;
    [Tooltip("Use linear interpolation. If unchecked, will use SmoothDamp().")]
    public bool linear;
    [Range(0f, 1f)]
    public float smoothTime = 0.5f;
    [Tooltip("Type of delta time used for SmoothDamp(). 1 for deltaTime, 2 for fixedDeltaTime, 3 for smoothDeltaTime.")]
    [Range(1, 3)]
    public int deltaType = 1;
    public bool useDeltaTimeInLinear;
    [Tooltip("Execute in LateUpdate(). If unchecked, code will execute in FixedUpdate()")]
    public bool useLateUpdate = true;
    [Tooltip("Camera velocity (output). Changes only when using SmoothDamp().")]

    private Vector3 velocity;

    private void Update() {
        // FOR DEBUGGING: reset follower's position
        if (Input.GetKeyDown(KeyCode.F))
            transform.position = Vector3.zero + offset;
    }

    private void FixedUpdate() {
        if (!useLateUpdate)
            Follow();
    }

    private void LateUpdate() {
        if (useLateUpdate)
            Follow();
    }

    private void Follow() {
        Vector3 targetPos = target.position + offset;
        float deltaTime;

        switch (deltaType) {
            case 1:
                deltaTime = Time.deltaTime;
                break;
            case 2:
                deltaTime = Time.fixedDeltaTime;
                break;
            case 3:
                deltaTime = Time.smoothDeltaTime;
                break;
            default:
                Debug.Log("delta time type must be within the 1-3 range.");
                return;
        }

        if (exact) {
            transform.position = targetPos;
        } else if (linear) {
            float _smoothTime;
        
            if (useDeltaTimeInLinear)
                _smoothTime = 1 - Mathf.Exp(-smoothTime * 10 * deltaType);
            else
                _smoothTime = smoothTime;

            transform.position = Vector3.Lerp(transform.position, targetPos, _smoothTime);
        } else {
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime, Mathf.Infinity, deltaTime);
        }
    }
}
