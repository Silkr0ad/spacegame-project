﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryScreen : MonoBehaviour {
    public GameObject panel;
    private PlayerMovement pm;
    private TimeKeeper tk;

    private void Awake() {
        tk = GetComponent<TimeKeeper>();
        pm = GetComponent<PlayerMovement>();
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.name == "Finish Line") {
            Time.timeScale = 0.1f;
            panel.SetActive(true);
            panel.transform.GetChild(1).GetComponent<Text>().text = tk.time;
            tk.HideTimer();
            StartCoroutine(Blink());
            pm.Won();
        }
    }

    private IEnumerator Blink() {
        float start = Time.time;
        float elapsed;
        while (true) {
            elapsed = (Time.time - start) / Time.timeScale;
            if ((int)elapsed % 2 == 0)
                panel.SetActive(true);
            else
                panel.SetActive(false);
            yield return null;
        }
    }
}
