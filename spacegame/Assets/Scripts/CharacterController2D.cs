﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterController2D : MonoBehaviour {
    // "public" fields
    [Header("Rotation\n")]
    [SerializeField] private float rotationSpeed = 10f;
    [SerializeField] private float molasses = 2f;
    [Header("Acceleration\n")]
    [SerializeField] private float acceleration = 40f;
    [SerializeField] private float topSpeed = 30f;
    [Header("Durability\n")]
    public float endurance;
    public ParticleSystem explosion;
    public GameObject panel;

    // private fields
    private float axis;
    private float _molasses;
    private int ind;
    // references
    private Rigidbody2D rb;

    private void Start() {
        // caching references
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        float damage = Mathf.Abs(Vector2.Dot(-rb.velocity, collision.GetContact(0).normal));
        if (damage >= endurance) {
            explosion.transform.position = transform.position;
            explosion.Play();
            explosion.GetComponent<AudioSource>().Play();
            rb.velocity = Vector2.zero;
            panel.SetActive(true);
            Destroy(gameObject);
        }
    }

    public void Rotate(float axis, bool isAccelerating) {
        float angle = (rotationSpeed / _molasses) * axis * (-1);
        rb.MoveRotation(rb.rotation + angle);
    }

    public void Accelerate() {
        rb.AddForce(transform.up * acceleration / _molasses);
    }

    private IEnumerator Acceleration() {
        yield return new WaitForEndOfFrame();
        rb.velocity = Vector2.Lerp(rb.velocity, transform.up * topSpeed, 0.05f);
    }

    public void LimitSpeed() {
        if (rb.velocity.magnitude > topSpeed)
            rb.velocity = rb.velocity.normalized * topSpeed;
    }

    public void SetMolassesActive(bool isActive) {
        if (isActive)
            _molasses = molasses;
        else
            _molasses = 1f;
    }
}