﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkSpawner : MonoBehaviour {
    public ParticleSystem sparks;
    public AudioSource scraping;
    private Rigidbody2D rb;

    public float minSpeed;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start() {
        sparks.Stop();
    }

    private void OnCollisionStay2D(Collision2D collision) {
        if (rb.velocity.magnitude > minSpeed) {
            Vector2 point = collision.GetContact(0).point;
            sparks.transform.position = point;
            if (!sparks.isPlaying) {
                sparks.Play();
                scraping.Play();
            }
        } else {
            if (sparks.isPlaying) {
                sparks.Stop();
                scraping.Pause();
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        if (sparks.isPlaying) { 
            sparks.Stop();
            scraping.Pause();
        }
    }
}
