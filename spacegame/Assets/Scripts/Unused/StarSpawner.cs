﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSpawner : MonoBehaviour {
    public Transform parent;
    public GameObject starPrefab;
    public float chanceOfSPawning = 0.4f;
    public float minSize;
    public float maxSize;

    private Camera cam;

    private void Awake() {
        cam = Camera.main;
    }

    void Start() {
        float aspect = cam.aspect;
        float camSize = cam.orthographicSize;

        float width = camSize * aspect;
        float height = camSize;

        for (int i = Mathf.FloorToInt(-width); i < Mathf.CeilToInt(width); i++) {
            for (int j = Mathf.CeilToInt(height); j > Mathf.FloorToInt(-height); j--) {
                if (Random.Range(0f, 1f) <= chanceOfSPawning) {
                    float size = Random.Range(minSize, maxSize);
                    Transform star = (Instantiate(starPrefab, parent) as GameObject).transform;
                    star.localPosition = new Vector3(i + 0.5f, j - 0.5f, 0f);
                    star.localScale = new Vector3(size, size, 1f);
                }
            }
        }
    }
}
