﻿Shader "Unlit/Stars using noise"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color ("Shade of Black", Color) = (0, 0, 0, 1.0)
		_Threshold ("Threshold", float) = 0.5
		_SecondThreshold ("Threshold", float) = 0.002
		_SecondBrightness ("Secondary Star Brightness", float) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

			struct appdata
			{
				float2 uv : TEXCOORD0;
				float4 vertex : POSITION;
			};

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			fixed4 _Color;
			float _Threshold;
			float _SecondThreshold;
			float _SecondBrightness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 col = _Color;
				float noise = tex2D(_MainTex, i.uv);
				if (noise > _Threshold)
					col.rgb += noise;
				else if (noise > _Threshold - _SecondThreshold)
					col.rgb += noise * _SecondBrightness;
				return col;
            }
            ENDCG
        }
    }
}
