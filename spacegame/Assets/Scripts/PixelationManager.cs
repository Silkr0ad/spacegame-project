﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelationManager : MonoBehaviour {
    public Int pixelationSO;

    private Camera cam;
    private Material material;
    private float aspect;

    private void Awake() {
        material = GetComponent<ImageEffectRenderer>().material;
        cam = Camera.main;
    }

    private void Start() {
        AdjustMaterial();
    }

    private void Update() {
        HandleInput();
        AdjustMaterial();
    }

    private void AdjustMaterial() {
        aspect = cam.aspect;
        int ypv = (int)Mathf.Round(pixelationSO.value / aspect); // ypv = y pixelation value
        material.SetInt("_Columns", pixelationSO.value);
        material.SetInt("_Rows", ypv);
    }

    private void HandleInput() {
        // a way to control pixelation in-game; for debugging only
        if (Input.GetKeyDown(KeyCode.M)) {
            if (Input.GetKey(KeyCode.LeftControl))
                pixelationSO.value += 2;
            else
                pixelationSO.value += 10;
        } else if (Input.GetKeyDown(KeyCode.N)) {
            if (Input.GetKey(KeyCode.LeftControl))
                pixelationSO.value -= 2;
            else
                pixelationSO.value -= 10;
        }
    }
}
